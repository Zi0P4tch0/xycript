/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  The MIT License (MIT)                                                          //
//                                                                                 //
//  Copyright (c) 2014 Matteo Pacini                                               //
//                                                                                 //
//  Permission is hereby granted, free of charge, to any person obtaining a copy   //
//  of this software and associated documentation files (the "Software"), to deal  //
//  in the Software without restriction, including without limitation the rights   //
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
//  copies of the Software, and to permit persons to whom the Software is          //
//  furnished to do so, subject to the following conditions:                       //
//                                                                                 //
//  The above copyright notice and this permission notice shall be included in     //
//  all copies or substantial portions of the Software.                            //
//                                                                                 //
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
//  THE SOFTWARE.                                                                  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

#import "XCSettings.h"

#import <objc/runtime.h>

@implementation XCSettings

#pragma mark -
#pragma mark - Lifecycle

+(instancetype)sharedSettings
{
    static XCSettings *_sharedSettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedSettings = [[XCSettings alloc] init];
    });
    return _sharedSettings;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSArray *paths =
            NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory,
                                                NSUserDomainMask,
                                                YES);
        
        NSString *xycriptDirPath =
        [[paths firstObject]
          stringByAppendingPathComponent:@"Xycript"];
        
        NSString *xycriptSettingsPath =
        [xycriptDirPath stringByAppendingPathComponent:@"Settings.plist"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:xycriptSettingsPath
                                                  isDirectory:nil])
        {
            //Populating properties using default values
            
            self.windowWidth = @600;
            self.windowHeight = @400;
            
            //Saving
            
            [[NSFileManager defaultManager] createDirectoryAtPath:xycriptDirPath
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:nil];
            
            NSLog(@"[XYCRIPT] Writing settings to file (first time).");
            NSLog(@"[XYCRIPT] %@",[self propertiesDict]);
            
            [[self propertiesDict] writeToFile:xycriptSettingsPath atomically:YES];
            
        }
        else
        {
            //Populating properties from saved plist
            
            NSLog(@"[XYCRIPT] Reading settings from file.");
            
            NSDictionary *propertiesDict =
            [NSDictionary dictionaryWithContentsOfFile:xycriptSettingsPath];
            
            NSLog(@"[XYCRIPT] %@", propertiesDict);
            
            [self restorePropertiesFromDict:propertiesDict];
        }
        
    }
    return self;
}

#pragma mark -
#pragma mark - Save

-(void)save
{
    NSLog(@"[XYCRIPT] Saving settings.");
    
    NSArray *paths =
    NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory,
                                        NSUserDomainMask,
                                        YES);
    
    NSString *xycriptDirPath =
    [[paths firstObject]
     stringByAppendingPathComponent:@"Xycript"];
    
    NSString *xycriptSettingsPath =
    [xycriptDirPath stringByAppendingPathComponent:@"Settings.plist"];
    
    NSLog(@"[XYCRIPT] Writing settings to file.");
    NSLog(@"[XYCRIPT] %@",[self propertiesDict]);
    
    [[self propertiesDict] writeToFile:xycriptSettingsPath atomically:YES];

}

#pragma mark -
#pragma mark - Utilities

-(void)restorePropertiesFromDict:(NSDictionary*)dict
{
    __weak typeof(self) __self = self;
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
       
        [__self setValue:obj forKey:key];
        
    }];
}

-(NSDictionary*)propertiesDict
{
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
    
    unsigned int outCount, i;

    objc_property_t *properties = class_copyPropertyList([XCSettings class], &outCount);
    
    for (i = 0; i < outCount; i++)
    {
        objc_property_t property = properties[i];
        
        const char *propName = property_getName(property);
        
        if (propName)
        {
            NSString *propertyName = [NSString stringWithUTF8String:propName];
            id propertyValue = [self valueForKey:propertyName];
            
            [tmp setObject:propertyValue forKey:propertyName];
        }
    }
    
    free(properties);
        
    return [NSDictionary dictionaryWithDictionary:tmp];
}

@end
