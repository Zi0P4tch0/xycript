/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  The MIT License (MIT)                                                          //
//                                                                                 //
//  Copyright (c) 2014 Matteo Pacini                                               //
//                                                                                 //
//  Permission is hereby granted, free of charge, to any person obtaining a copy   //
//  of this software and associated documentation files (the "Software"), to deal  //
//  in the Software without restriction, including without limitation the rights   //
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
//  copies of the Software, and to permit persons to whom the Software is          //
//  furnished to do so, subject to the following conditions:                       //
//                                                                                 //
//  The above copyright notice and this permission notice shall be included in     //
//  all copies or substantial portions of the Software.                            //
//                                                                                 //
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
//  THE SOFTWARE.                                                                  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

#import "NSAlert+TextField.h"

@implementation NSAlert(TextField)

+(void)showAlertWithMessage:(NSString*)message
{
    NSAlert *alert = [NSAlert alertWithMessageText:message
                                     defaultButton:@"Dismiss"
                                   alternateButton:nil
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    
    [alert runModal];
}

+(void)showInputAlertWithMessage:(NSString*)message
                             okButton:(NSString*)okButton
                        dismissButton:(NSString*)dismissButton
                     inputPlaceholder:(NSString*)placeholder
                             onReturn:(void (^)(NSString*))returnBlock
                             onCancel:(void (^)())cancelBlock
                               secure:(BOOL)secure
{
    NSAlert *alert = [NSAlert alertWithMessageText:message
                                     defaultButton:okButton
                                   alternateButton:dismissButton
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    
    Class textFieldClass = secure?[NSSecureTextField class]:[NSTextField class];
    
    NSTextField *input = [[textFieldClass alloc] initWithFrame:NSMakeRect(0, 0, 200, 24)];
    if (placeholder)
    {
        [input setStringValue:placeholder];
    }
    
    [alert setAccessoryView:input];
    
    NSInteger result = [alert runModal];
    
    if (result == NSAlertDefaultReturn && returnBlock)
    {
        [input validateEditing];
        returnBlock(input.stringValue);
    }
    
    if (result == NSAlertAlternateReturn && cancelBlock)
    {
        cancelBlock();
    }
}

@end
