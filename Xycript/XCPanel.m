/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  The MIT License (MIT)                                                          //
//                                                                                 //
//  Copyright (c) 2014 Matteo Pacini                                               //
//                                                                                 //
//  Permission is hereby granted, free of charge, to any person obtaining a copy   //
//  of this software and associated documentation files (the "Software"), to deal  //
//  in the Software without restriction, including without limitation the rights   //
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
//  copies of the Software, and to permit persons to whom the Software is          //
//  furnished to do so, subject to the following conditions:                       //
//                                                                                 //
//  The above copyright notice and this permission notice shall be included in     //
//  all copies or substantial portions of the Software.                            //
//                                                                                 //
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
//  THE SOFTWARE.                                                                  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

#import "XCPanel.h"

#import "XCConsoleTextView.h"
#import "XCSettings.h"

@interface XCPanel()<NSWindowDelegate>

@property (nonatomic, strong) XCConsoleTextView *consoleTextView;

@end

@implementation XCPanel

#pragma mark -
#pragma mark - Lifecycle

-(instancetype)initWithIPAddress:(NSString*)ipAddress
                    rootPassword:(NSString*)rootPassword
{
    int styles = NSClosableWindowMask | NSTitledWindowMask;
    
    CGRect contentRect = CGRectMake(0,
                                    0,
                                    [[[XCSettings sharedSettings] windowWidth] integerValue],
                                    [[[XCSettings sharedSettings] windowHeight] integerValue]);
    
    self = [super initWithContentRect:contentRect
                            styleMask:styles
                              backing:NSBackingStoreBuffered
                                defer:NO];
    
    if (self)
    {
        [self setup];
        
    }
    
    return self;
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    NSLog(@"[XYCRIPT] Window will close.");
}

#pragma mark -
#pragma mark - Setup

-(void)setup
{
    self.title = [NSString stringWithFormat:@"Xycript v%@ (\"%@\")",VERSION,CODENAME];
    
    self.consoleTextView = [[XCConsoleTextView alloc] initWithFrame:self.frame];
        
    [self setContentView:self.consoleTextView];
    
    self.delegate = self;
}

@end
