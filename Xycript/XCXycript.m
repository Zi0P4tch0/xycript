/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  The MIT License (MIT)                                                          //
//                                                                                 //
//  Copyright (c) 2014 Matteo Pacini                                               //
//                                                                                 //
//  Permission is hereby granted, free of charge, to any person obtaining a copy   //
//  of this software and associated documentation files (the "Software"), to deal  //
//  in the Software without restriction, including without limitation the rights   //
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
//  copies of the Software, and to permit persons to whom the Software is          //
//  furnished to do so, subject to the following conditions:                       //
//                                                                                 //
//  The above copyright notice and this permission notice shall be included in     //
//  all copies or substantial portions of the Software.                            //
//                                                                                 //
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
//  THE SOFTWARE.                                                                  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

#import "XCXycript.h"

#include <arpa/inet.h>

#import "XCSettings.h"
#import "XCPanel.h"

static XCXycript *sharedPlugin;

@interface XCXycript()

@property (nonatomic, strong) NSBundle *bundle;

@property (nonatomic, strong) XCPanel *xcpanel;

@end

@implementation XCXycript

#pragma mark -
#pragma mark - Lifecycle

+(void)pluginDidLoad:(NSBundle *)plugin
{
    static dispatch_once_t onceToken;
    NSString *currentApplicationName = [[NSBundle mainBundle] infoDictionary][@"CFBundleName"];
    if ([currentApplicationName isEqual:@"Xcode"]) {
        dispatch_once(&onceToken, ^{
            sharedPlugin = [[self alloc] initWithBundle:plugin];
        });
    }
}

- (id)initWithBundle:(NSBundle *)plugin
{    
    if (self = [super init])
    {
        self.bundle = plugin;
        
        //Product menu
        NSMenuItem *productMenuItem = [[NSApp mainMenu] itemWithTitle:@"Product"];
        
        if (productMenuItem)
        {
            //Add a separator
            [[productMenuItem submenu] addItem:[NSMenuItem separatorItem]];
            
            //Add Xycript menu item
            NSMenuItem *cycriptMenuItem =
            [[NSMenuItem alloc] initWithTitle:@"Xycript"
                                       action:@selector(cycript)
                                keyEquivalent:@""];
            [cycriptMenuItem setTarget:self];
            
            [[productMenuItem submenu] addItem:cycriptMenuItem];
        }
        
    }
    return self;
}

-(void)dealloc
{
    NSLog(@"[XYCRIPT] Dealloc.");
    
    [[XCSettings sharedSettings] save];
}

#pragma mark -
#pragma mark - Actions

-(void)cycript
{
    NSLog(@"[XYCRIPT] Xycript button has been clicked.");
    
    __block NSString *__ipAddress = nil;
    
    __weak typeof(self) __self = self;
    
    ///////////////////////////
    // PASSWORD RETURN BLOCK //
    ///////////////////////////
    
    void (^passwordReturnBlock)(NSString*) = ^(NSString* password){
        
        __self.xcpanel = [[XCPanel alloc] initWithIPAddress:__ipAddress
                                                 rootPassword:password];
        
        [[NSApp mainWindow] addChildWindow:__self.xcpanel ordered:NSWindowAbove];
        
        [__self.xcpanel center];
                
    };
    
    /////////////////////////////
    // IP ADDRESS RETURN BLOCK //
    /////////////////////////////
    
    void (^returnBlock)(NSString*) = ^(NSString* ipAddress){
        
        if(![__self isIPAddressValid:ipAddress])
        {
            [NSAlert showAlertWithMessage:@"Invalid IP Address!"];
        }
        else
        {
            __ipAddress = ipAddress;
            
            [NSAlert showInputAlertWithMessage:@"Enter your root password:"
                                      okButton:@"OK"
                                 dismissButton:@"Cancel"
                              inputPlaceholder:@"alpine"
                                      onReturn:passwordReturnBlock
                                      onCancel:nil
                                        secure:YES];

        }
        
    };
    
    [NSAlert showInputAlertWithMessage:@"Enter your jailbroken iPhone IP address:"
                              okButton:@"OK"
                         dismissButton:@"Cancel"
                      inputPlaceholder:@"e.g. 192.168.1.2"
                              onReturn:returnBlock
                              onCancel:nil
                                secure:NO];

}

#pragma mark -
#pragma mark - Utilities

-(BOOL)isIPAddressValid:(NSString*)ipAddress
{
    const char *utf8 = [ipAddress UTF8String];
    int success;
    
    struct in_addr dst;
    success = inet_pton(AF_INET, utf8, &dst);
    if (success != 1) {
        struct in6_addr dst6;
        success = inet_pton(AF_INET6, utf8, &dst6);
    }
    
    return (success == 1 ? YES : NO);
}

@end
