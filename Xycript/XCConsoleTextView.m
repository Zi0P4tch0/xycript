/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  The MIT License (MIT)                                                          //
//                                                                                 //
//  Copyright (c) 2014 Matteo Pacini                                               //
//                                                                                 //
//  Permission is hereby granted, free of charge, to any person obtaining a copy   //
//  of this software and associated documentation files (the "Software"), to deal  //
//  in the Software without restriction, including without limitation the rights   //
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
//  copies of the Software, and to permit persons to whom the Software is          //
//  furnished to do so, subject to the following conditions:                       //
//                                                                                 //
//  The above copyright notice and this permission notice shall be included in     //
//  all copies or substantial portions of the Software.                            //
//                                                                                 //
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
//  THE SOFTWARE.                                                                  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

#import "XCConsoleTextView.h"

typedef enum
{
    XCLogInfoStyle,
    XCLogWarningStyle,
    XCLogErrorStyle
    
} XCLogStyle;

@implementation XCConsoleTextView

#pragma mark -
#pragma mark - Lifecycle

-(instancetype)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self)
    {
        [self setup];
    }
    return self;
}

#pragma mark -
#pragma mark - Setup

-(void)setup
{
    self.backgroundColor = [NSColor darkGrayColor];
    self.textColor = [NSColor whiteColor];
    
    self.editable = NO;
    self.selectable = NO;
}

#pragma mark -
#pragma mark - Logging

-(NSDictionary*)attributesForStyle:(XCLogStyle)style
{
    switch (style) {
        case XCLogInfoStyle:
            return @{ NSForegroundColorAttributeName : [NSColor whiteColor] };
            break;
        case XCLogWarningStyle:
            return @{ NSForegroundColorAttributeName : [NSColor orangeColor] };
            break;
        case XCLogErrorStyle:
            return @{ NSForegroundColorAttributeName : [NSColor redColor] };
            break;
            
    }
}

-(void)log:(NSString*)msg style:(XCLogStyle)style
{
    
    NSAttributedString *attrMsg =
    [[NSAttributedString alloc] initWithString:msg
                                    attributes:[self attributesForStyle:style]];
    
    [[self textStorage] appendAttributedString:attrMsg];
    
    [self scrollRangeToVisible:
     NSMakeRange([self.attributedString length] - 1, 1)];
}

-(void)info:(NSString*)msg
{
    return [self log:msg style:XCLogInfoStyle];
}

-(void)warning:(NSString*)msg
{
    return [self log:msg style:XCLogWarningStyle];
}

-(void)error:(NSString*)msg
{
    return [self log:msg style:XCLogErrorStyle];
}

@end
